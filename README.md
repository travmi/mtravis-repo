# repo

#### Table of Contents

1. [Overview](#overview)
2. [Setup - The basics of getting started with repo](#setup)
    * [What repo affects](#what-repo-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with repo](#beginning-with-repo)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)
7. [License - License information](#license)

##Overview

The repo module manages the mirror-wcw repo and centos repos.
By default all the centos repos are disabled. 

This module needs some work in order to enable/disable repos through profiles.

##Setup

###What repo affects
* mirror-wcw repo
* centos repos

###Beginning with repo

```puppet
include repo
```

##Usage

All interaction with the repo module can do be done through the main repo class.

###I just want repo, what's the minimum I need?

```puppet
include repo
```

##Reference

###Classes

####Public Classes

* repo: Main class, includes all other classes.

####Private Classes

* repo::centos: Handles Centos repos.
* repo::gpg: Handles the GPG keys for the repos.

###Parameters for repo

The following parameters are available in the repo module:

####There are currently no paramters for this class.

##Limitations

This module has been built on and tested against Puppet Enterprise 3.3 and higher.

The module has been tested on:

* CentOS 6.5

Support on other platforms has not been tested and is currently not supported. 

##Development

###License
Copyright (C) 2014 Mike Travis

For the relevant commits Copyright (C) by the respective authors.

Can be contacted at: mike.r.travis@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.