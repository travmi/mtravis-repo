# Class: repo::centos
# 
# This module manages the Centos repos.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# This class is not called directly. 
class repo::centos {
  
  yumrepo { 'C6.0-base':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.0/os/$basearch/',
    descr    => 'CentOS-6.0 - Base',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.0-centosplus':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.0/centosplus/$basearch/',
    descr    => 'CentOS-6.0 - CentOSPlus',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.0-contrib':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.0/contrib/$basearch/',
    descr    => 'CentOS-6.0 - Contrib',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.0-extras':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.0/extras/$basearch/',
    descr    => 'CentOS-6.0 - Extras',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.0-updates':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.0/updates/$basearch/',
    descr    => 'CentOS-6.0 - Updates',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.1-base':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.1/os/$basearch/',
    descr    => 'CentOS-6.1 - Base',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.1-centosplus':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.1/centosplus/$basearch/',
    descr    => 'CentOS-6.1 - CentOSPlus',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.1-contrib':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.1/contrib/$basearch/',
    descr    => 'CentOS-6.1 - Contrib',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.1-extras':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.1/extras/$basearch/',
    descr    => 'CentOS-6.1 - Extras',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.1-updates':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.1/updates/$basearch/',
    descr    => 'CentOS-6.1 - Updates',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.2-base':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.2/os/$basearch/',
    descr    => 'CentOS-6.2 - Base',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.2-centosplus':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.2/centosplus/$basearch/',
    descr    => 'CentOS-6.2 - CentOSPlus',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.2-contrib':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.2/contrib/$basearch/',
    descr    => 'CentOS-6.2 - Contrib',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.2-extras':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.2/extras/$basearch/',
    descr    => 'CentOS-6.2 - Extras',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.2-updates':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.2/updates/$basearch/',
    descr    => 'CentOS-6.2 - Updates',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.3-base':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.3/os/$basearch/',
    descr    => 'CentOS-6.3 - Base',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.3-centosplus':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.3/centosplus/$basearch/',
    descr    => 'CentOS-6.3 - CentOSPlus',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.3-contrib':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.3/contrib/$basearch/',
    descr    => 'CentOS-6.3 - Contrib',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.3-extras':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.3/extras/$basearch/',
    descr    => 'CentOS-6.3 - Extras',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'C6.3-updates':
    ensure   => 'present',
    baseurl  => 'http://vault.centos.org/6.3/updates/$basearch/',
    descr    => 'CentOS-6.3 - Updates',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'base':
    ensure     => 'present',
    descr      => 'CentOS-$releasever - Base',
    enabled    => '0',
    gpgcheck   => '0',
    gpgkey     => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
    mirrorlist => 'http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=os',
  }
  yumrepo { 'c6-media':
    ensure   => 'present',
    baseurl  => 'file:///media/CentOS/
                 file:///media/cdrom/
                 file:///media/cdrecorder/',
    descr    => 'CentOS-$releasever - Media',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
  }
  yumrepo { 'centosplus':
    ensure     => 'present',
    descr      => 'CentOS-$releasever - Plus',
    enabled    => '0',
    gpgcheck   => '0',
    gpgkey     => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
    mirrorlist => 'http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=centosplus',
  }
  yumrepo { 'contrib':
    ensure     => 'present',
    descr      => 'CentOS-$releasever - Contrib',
    enabled    => '0',
    gpgcheck   => '0',
    gpgkey     => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6',
    mirrorlist => 'http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=contrib',
  }
  yumrepo { 'debug':
    ensure   => 'present',
    baseurl  => 'http://debuginfo.centos.org/6/$basearch/',
    descr    => 'CentOS-6 - Debuginfo',
    enabled  => '0',
    gpgcheck => '0',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-Debug-6',
  }
}