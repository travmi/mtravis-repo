# Class: repo::gpg
#
# This module manages the GPG keys for the Centos repos.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# This class is not called directly.
class repo::gpg {
  
  file { '/etc/pki/rpm-gpg':
    path    => '/etc/pki/rpm-gpg',
    ensure  => 'directory',
    source  => 'puppet:///modules/repo/gpg',
    recurse => true,
    replace => true,
    mode    => '0644',
  }
  
}