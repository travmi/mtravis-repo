# == Class: repo
#
# This module manages the WCW Mirror Repo, Webtatic and EPEL repos.
#
# === Examples
#
# Using the follwing will only install the WCW repo.
#
# include repo
#
class repo (
  
  $wcw      = 'present',
  $epel     = 'absent',
  $webtatic = 'absent',
  $foreman  = 'absent'
  
){

#  file { '/etc/yum.repos.d/':
#    ensure  => 'directory',
#    mode    => '0644',
#    recurse => true,
#    purge   => true
#  }

   file { '/etc/yum.repos.d/mirror-wcw.repo':
     ensure => 'present',
     mode   => '0644',
     source => 'puppet:///modules/repo/mirror-wcw.repo',
    }

#    file { '/etc/yum.repos.d/epel.repo':
#      ensure => 'present',
#      mode   => '0644',
#      source => 'puppet:///modules/repo/epel.repo',
#    }
  
#    file { '/etc/yum.repos.d/foreman.repo':
#      ensure => 'present',
#      mode   => '0644',
#      source => 'puppet:///modules/repo/foreman.repo',
#    }

#    file { '/etc/yum.repos.d/webtatic.repo':
#      ensure => 'present',
#      mode   => '0644',
#      source => 'puppet:///modules/repo/webtatic.repo',
#    } 

}
